#!/bin/bash -x 

if [ "$#" -ne 1 ]; then    
	echo "Usage: publish-update-site.sh " 
else 	
	REF_NAME=$1 	
	RELEASE_REGEX=^release-v.*$ 	
	if [ "$REF_NAME" == "master" ] || [[ "$REF_NAME" =~ $RELEASE_REGEX ]]; then 
		if [ "$REF_NAME" == "master" ]; then
			VERSION=latest 		
		else 			
			EMPTYVAR="" 			
			VERSION=${REF_NAME/release-v/$EMPTYVAR} 		
		fi 
		
		echo "Downloading artifacts. Version is $VERSION." 		
		curl --insecure --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v3/projects/2635595/builds/artifacts/$REF_NAME/download?job=maven_build" > update-site.zip 		
		PUBLISH_DIR=public/update-site/$VERSION 		
		rm -rf $PUBLISH_DIR 		
		mkdir -p $PUBLISH_DIR 		
		unzip update-site.zip 		
		cp -R src/de.ragedev.race.releng.p2.orbit/target/repository/* $PUBLISH_DIR 		
		rm -rf src 		
		rm -rf update-site.zip 	
	else 		
		echo "Warning publishing not executed. Reason: Trigger job REF must be master or match $RELEASE_REGEX." 	
	fi 
fi 
